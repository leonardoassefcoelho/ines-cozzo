<?php
get_header();
?>

<div class="error-content top-header-distance padding-container">
    <h2 class="error">404</h2>
    <p class="description">Essa página não foi encontrada...</p>

    <a class="go-back-link" href="<?= get_home_url() ?>">Voltar para a home</a>
</div>

<?php
get_footer();
