<?php /* Template Name: Home */
get_header();
?>

<main class="home-content">
    <?php get_template_part('template-parts/home/hero');  ?>
    <?php get_template_part('template-parts/home/sobre');  ?>
    <?php get_template_part('template-parts/home/palestras');  ?>
    <?php get_template_part('template-parts/home/consultoria');  ?>
    <?php get_template_part('template-parts/home/cursos'); ?>
    <?php get_template_part('template-parts/home/contato'); ?>
    <?php get_template_part('template-parts/home/whatsapp-bubble'); ?>
</main>

<?php
get_footer();
