<!DOCTYPE html>
<html <?= get_language_attributes() ?>>

<head>
    <?php
    $template_directory_uri = get_template_directory_uri();
    $gtm_id = get_field("gtm-id", "option");
    $header_logo = get_field("header-logo", "option");

    if ($gtm_id) :
    ?>
        <!-- Google Tag Manager -->
        <script>
            (function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', '<?= $gtm_id ?>');
        </script>
        <!-- End Google Tag Manager -->
    <?php
    endif;
    echo get_field("tags-field", 'options');
    ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $template_directory_uri ?>/assets/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $template_directory_uri ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $template_directory_uri ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= $template_directory_uri ?>/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?= $template_directory_uri ?>/assets/favicon/safari-pinned-tab.svg" color="#c61017">
    <link rel="shortcut icon" href="<?= $template_directory_uri ?>/assets/favicon/favicon.ico">
    <meta name="msapplication-config" content="<?= $template_directory_uri ?>/assets/favicon/browserconfig.xml">
    <meta name="msapplication-TileColor" content="#231f20">
    <meta name="theme-color" content="#c61017">
    <script async src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
    <?php wp_head() ?>
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Person",
            "name": "<?= get_bloginfo('name') ?>"
            "url": "<?= home_url() ?>",
        }
    </script>
</head>

<body <?php body_class() ?>>
    <?php
    if ($gtm_id) :
    ?>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= $gtm_id ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    <?php
    endif;
    ?>
    <header>
        <div class="padding-container header">
            <?php if (is_front_page()) : ?>
                <h1 class="logo">
                <?php else : ?>
                    <a class="logo" href="<?= home_url() ?>" aria-label="Home">
                    <?php endif; ?>
                    <?php
                    if ($header_logo) :
                        echo file_get_contents($header_logo);
                    endif;
                    ?>
                    <?php if (is_front_page()) : ?>
                </h1>
            <?php else : ?>
                </a>
            <?php endif; ?>
            <div class="header-items-wrapper">
                <nav class="header-menu tablet desktop" aria-label="Main menu">
                    <h2 class="hidden">
                        Main Menu
                    </h2>
                    <?php wp_nav_menu(array(
                        'menu'           => 'Header Menu',
                        'theme_location' => 'header-menu',
                        'fallback_cb'    => false,
                    )); ?>
                </nav>
            </div>
            <button class="menu-button mobile" aria-label="Menu">
                <span class="menu-part"></span>
                <span class="menu-part"></span>
                <span class="menu-part"></span>
            </button>
        </div>

        <div class="padding-container menu-wrapper mobile">
            <nav class="header-menu">
                <h2 class="hidden">
                    Main Menu
                </h2>
                <?php wp_nav_menu(array(
                    'menu'           => 'Header Menu',
                    'theme_location' => 'header-menu',
                    'fallback_cb'    => false,
                )); ?>
            </nav>
        </div>
    </header>