<?php
$hero_video = get_field('hero-background-video');
$hero_video_webm = get_field('hero-background-video-webm');
$video_placeholder = ines_cozzo_image_sanitize(get_field('hero-background-placeholder'), "full");
$primary_button_text = get_field('hero-main-button-text');
$secondary_button_text = get_field('hero-secondary-button-text');
?>
<section class="home-hero" id="home">
    <video poster="<?= $video_placeholder['src'] ?>" autoplay loop muted playsinline>
        <?php if ($hero_video_webm && $hero_video_webm != "") : ?>
            <source src="<?= $hero_video_webm ?>" type="video/webm">
        <?php
        endif;
        if ($hero_video && $hero_video != "") :
        ?>
            <source src="<?= $hero_video ?>" type="video/mp4">
        <?php endif; ?>
        Desculpe, o seu navegador não suporta vídeos incorporados,
        mas você pode <a href="<?= $hero_video ?>">baixá-lo</a>
        e assistir pelo seu reprodutor de mídia favorito!
    </video>
    <div class="content padding-container">
        <h2 class="title"><?= get_field('hero-title') ?></h2>
        <p class="description"><?= get_field('hero-description') ?></p>
        <?php if ($primary_button_text || $secondary_button_text) : ?>
            <div class="buttons-wrapper">
                <?php if ($primary_button_text && $primary_button_text != "") : ?>
                    <a class="button primary" href="<?= get_field('hero-main-button-url') ?>"><?= $primary_button_text ?></a>
                <?php endif; ?>
                <?php if ($secondary_button_text && $secondary_button_text != "") : ?>
                    <a class="button secondary" href="<?= get_field('hero-secondary-button-url') ?>">
                        <?= $secondary_button_text . file_get_contents(get_template_directory_uri() . '/assets/icons/double-arrow.svg') ?>
                    </a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</section>