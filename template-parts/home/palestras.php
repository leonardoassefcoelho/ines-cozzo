<section class="palestras-wrapper" id="palestras">
    <div class="content padding-container">
        <h2 class="title"><?= get_field('home-palestras-title') ?></h2>
        <p class="subtitle"><?= get_field('home-palestras-subtitle') ?></p>
        <?=  get_field('home-palestras-video'); ?>
    </div>
    <div class="videos-wrapper padding-container">
        <h3 class="title">Mais vídeos</h3>
        <?php
        if (have_rows('home-palestras-palestras-repeater')) :
        ?>
            <div class="videos-carousel">
                <?php
                while (have_rows('home-palestras-palestras-repeater')) : the_row();
                    $video_image = ines_cozzo_image_sanitize(get_sub_field('home-video-imagem'), "medium");
                ?>
                    <div class="video-slide">
                        <img loading="lazy" class="video-image cover" src="<?= $video_image['src'] ?>" alt="<?= $video_image['alt'] ?>" title="<?= $video_image['title'] ?>" <?= ($video_image['srcset'] ? 'srcset="' . $video_image['srcset'] . '"' : '') ?>>
                        <div class="content-wrapper">
                            <h4 class="title"><?= get_sub_field('home-video-titulo') ?></h4>
                            <p class="description"><?= get_sub_field('home-video-descricao') ?></p>
                            <a class="link" href="<?= get_sub_field('home-video-url') ?>" rel="nofollow" target="_blank">Ver vídeo</a>
                        </div>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
        <?php
        endif;
        ?>
    </div>
</section>