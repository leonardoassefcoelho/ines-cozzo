<section class="sobre-wrapper padding-container" id="sobre">
    <div class="content">
        <h2 class="title"><?= get_field('home-sobre-title') ?></h2>
        <p class="subtitle"><?= get_field('home-sobre-subtitle') ?></p>
        <p class="description"><?= get_field('home-sobre-text') ?></p>
    </div>
    <?php $sobre_image = ines_cozzo_image_sanitize(get_field('home-sobre-image'), "medium"); ?>
    <img loading="lazy" class="sobre-image" src="<?= $sobre_image['src'] ?>" alt="<?= $sobre_image['alt'] ?>" title="<?= $sobre_image['title'] ?>" <?= ($sobre_image['srcset'] ? 'srcset="' . $sobre_image['srcset'] . '"' : '') ?>>
    <div class="premios-wrapper">
        <h3 class="title">Prêmios e Reconhecimentos</h3>
        <?php
        if (have_rows('home-sobre-premios-repeater')) :
        ?>
            <div class="premios-carousel">
                <?php
                while (have_rows('home-sobre-premios-repeater')) : the_row();
                    $premio_image = ines_cozzo_image_sanitize(get_sub_field('home-hero-premio-imagem'), "medium");
                ?>
                    <div class="premio-slide">
                        <img loading="lazy" class="premio-image cover" src="<?= $premio_image['src'] ?>" alt="<?= $premio_image['alt'] ?>" title="<?= $premio_image['title'] ?>" <?= ($premio_image['srcset'] ? 'srcset="' . $premio_image['srcset'] . '"' : '') ?>>
                        <div class="content-wrapper">
                            <h4 class="title"><?= get_sub_field('home-hero-premio-titulo') ?></h4>
                            <p class="description"><?= get_sub_field('home-hero-premio-descricao') ?></p>
                        </div>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
        <?php
        endif;
        ?>
    </div>
</section>