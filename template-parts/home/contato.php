<section class="contato-wrapper padding-container" id="contato">
    <div class="content">
        <h2 class="title"><?= get_field('home-contato-title') ?></h2>
        <p class="subtitle"><?= get_field('home-contato-subtitle') ?></p>
        <p class="description"><?= get_field('home-contato-text') ?></p>
    </div>
    <form class="contact-form" action="<?= admin_url('admin-ajax.php') ?>?action=sendmail" method="post">
        <?php
        $contact_form_button_text = get_field('home-contact-form-button-text');
        wp_nonce_field("contact_form", "contact-nonce");
        ?>

        <label for="name">Nome</label>
        <input class="input" required id="name" name="name" placeholder="Seu nome completo" type="text" aria-label="Nome completo">

        <label for="email">E-mail</label>
        <input class="input" required id="email" name="email" placeholder="seu@email.com" type="email" aria-label="E-mail">

        <label for="phone">Telefone</label>
        <input class="input" required id="phone" name="phone" placeholder="(DDD) 12345-6789" type="phone" aria-label="Telefone">

        <label for="subject">Assunto</label>
        <input class="input" required id="subject" name="subject" placeholder="Orçamento para evento, sobre cursos, etc." type="text" aria-label="Assunto">

        <label for="message">Mensagem</label>
        <textarea id="message" required name="message" placeholder="Como posso te ajudar?" aria-label="Mensagem"></textarea>

        <input class="input city" id="city" name="city" type="text" aria-label="Cidade" value="">

        <button class="button primary" type="submit" aria-label="<?= $contact_form_button_text ?>"><?= $contact_form_button_text ?></button>
        <div class="form-modal" hidden>
            <div class="succcess-content">
                <lottie-player src="https://assets8.lottiefiles.com/packages/lf20_rtxcgnqq.json" background="transparent" speed="1" style="width: 221px; height: 193px;"></lottie-player>
                <p class="title">Sua mensagem foi enviada com sucesso.</p>
                <p class="description">Entraremos em contato com você o mais breve possível. Tenha um ótimo dia!</p>
                <a class="button primary" href="#home">Voltar ao início</a>
            </div>
            <div class="error-content" hidden>
                <lottie-player src="https://assets5.lottiefiles.com/packages/lf20_e1pmabgl.json" loop background="transparent" speed="1" style="width: 184px; height: 132px;"></lottie-player>
                <p class="title">Ops!<br>Algo aconteceu.</p>
                <p class="description">Sua mensagem não foi enviada. Por favor, tente enviá-la novamente.</p>
                <button class="close button primary">Tentar novamente</button>
                <a class="button secondary" href="#home">Voltar ao início</a>
            </div>
        </div>
    </form>
</section>