<section class="consultoria-wrapper" id="consultoria">
    <div class="content padding-container">
        <h2 class="title"><?= get_field('home-consultoria-title') ?></h2>
        <p class="subtitle"><?= get_field('home-consultoria-subtitle') ?></p>
        <p class="description"><?= get_field('home-consultoria-text') ?></p>
    </div>
    <div class="empresas-wrapper padding-container">
        <?php
        if (have_rows('home-consultoria-empresas-repeater')) :
            while (have_rows('home-consultoria-empresas-repeater')) : the_row();
                $empresa_image = ines_cozzo_image_sanitize(get_sub_field('home-consultoria-empresa-imagem'), "medium");
        ?>
                <img loading="lazy" class="empresa-image" src="<?= $empresa_image['src'] ?>" alt="<?= $empresa_image['alt'] ?>" title="<?= $empresa_image['title'] ?>" <?= ($empresa_image['srcset'] ? 'srcset="' . $empresa_image['srcset'] . '"' : '') ?>>
        <?php
            endwhile;
        endif;
        ?>
    </div>
    <?php
    $template_directory_uri = get_template_directory_uri();
    if (have_rows('home-consultoria-depoimentos-repeater')) :
    ?>
        <div class="depoimentos-wrapper padding-container">
            <?php
            while (have_rows('home-consultoria-depoimentos-repeater')) : the_row();
            ?>
                <div class="depoimento-slide">
                    <div class="content-wrapper">
                        <?= file_get_contents($template_directory_uri . "/assets/icons/aspas.svg") ?>
                        <p class="depoimento"><?= get_sub_field('home-depoimento-texto') ?></p>
                        <p class="nome"><?= get_sub_field('home-depoimento-nome') ?></p>
                        <p class="titulo"><?= get_sub_field('home-depoimento-titulo') ?></p>
                    </div>
                </div>
            <?php
            endwhile;
            ?>
        </div>
    <?php
    endif;

    $button_text = get_field('home-consultoria-button-text');

    if ($button_text && $button_text != "") :
    ?>
        <a class="button primary" href="<?= get_field('home-consultoria-button-url') ?>"><?= $button_text ?></a>
    <?php
    endif;
    ?>
</section>