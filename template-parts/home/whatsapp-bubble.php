<?php if (is_null(get_field('social-network-whatsapp-show')) || get_field('social-network-whatsapp-show')) : ?>
    <a class="whatsapp-bubble button primary" aria-label="Chamar no WhatsApp" rel="nofollow" target="_blank" href="<?= sprintf("https://wa.me/55%s?text=%s&?lang=pt_br", get_field('social-network-whatsapp-number', 'options'), urlencode(get_field('social-network-whatsapp-message', 'options'))) ?>">
        <?= file_get_contents(get_template_directory_uri() . "/assets/icons/whatsapp.svg") ?>
    </a>
<?php endif; ?>