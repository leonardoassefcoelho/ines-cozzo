    <?php
    if (have_rows('home-cursos-repeater')) : ?>
        <section class="cursos-wrapper container" id="cursos">
            <div class="content">
                <h2 class="title"><?= get_field('home-cursos-title') ?></h2>
                <p class="subtitle"><?= get_field('home-cursos-subtitle') ?></p>
            </div>
            <?php
            while (have_rows('home-cursos-repeater')) : the_row()
            ?>
                <div class="content-cursos" id="<?= get_sub_field('home-cursos-repeater-slug') ?>">
                    <div class="content-wrapper">
                        <h2 class="title"><?= get_sub_field('home-cursos-repeater-title') ?></h2>
                        <p class="subtitle"><?= get_sub_field('home-cursos-repeater-subtitle') ?></p>
                        <?php $cursos_voce_image = ines_cozzo_image_sanitize(get_sub_field('home-cursos-repeater-image'), "medium"); ?>
                        <img loading="lazy" class="content-image mobile" src="<?= $cursos_voce_image['src'] ?>" alt="<?= $cursos_voce_image['alt'] ?>" title="<?= $cursos_voce_image['title'] ?>" <?= ($cursos_voce_image['srcset'] ? 'srcset="' . $cursos_voce_image['srcset'] . '"' : '') ?>>
                        <p class="text"><?= get_sub_field('home-cursos-repeater-text') ?></p>
                        <?php
                        if (have_rows('home-cursos-repeater-repeater')) :
                            $template_directory_uri = get_template_directory_uri();
                        ?>
                            <ul class="lista">
                                <?php
                                while (have_rows('home-cursos-repeater-repeater')) : the_row();
                                ?>
                                    <li class="item-lista">
                                        <?= file_get_contents($template_directory_uri . '/assets/icons/check.svg') ?>
                                        <div class="lista-content">
                                            <p class="title"><?= get_sub_field('home-cursos-empresa-repeater-titulo') ?></h4>
                                            <p class="description"><?= get_sub_field('home-cursos-empresa-repeater-descricao') ?></p>
                                        </div>
                                    </li>
                                <?php
                                endwhile;
                                ?>
                            </ul>
                        <?php
                        endif;

                        $show_button = get_sub_field('home-cursos-repeater-button-show');

                        if ($show_button) :
                        ?>
                            <a class="button primary" href="<?= get_sub_field('home-cursos-repeater-button-url') ?>" target="<?= get_sub_field('home-cursos-repeater-button-target') ?>"><?= get_sub_field('home-cursos-repeater-button-text') ?></a>
                        <?php
                        endif;
                        ?>
                    </div>
                    <img loading="lazy" class="content-image tablet desktop" src="<?= $cursos_voce_image['src'] ?>" alt="<?= $cursos_voce_image['alt'] ?>" title="<?= $cursos_voce_image['title'] ?>" <?= ($cursos_voce_image['srcset'] ? 'srcset="' . $cursos_voce_image['srcset'] . '"' : '') ?>>
                </div>
            <?php
            endwhile; ?>
        </section>
    <?php
    endif;
    ?>