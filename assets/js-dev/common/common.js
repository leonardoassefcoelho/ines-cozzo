$ = jQuery.noConflict();

jQuery.event.special.touchstart = {
  setup: function (_, ns, handle) {
    this.addEventListener("touchstart", handle, {
      passive: !ns.includes("noPreventDefault"),
    });
  },
};

jQuery.event.special.touchmove = {
  setup: function (_, ns, handle) {
    this.addEventListener("touchmove", handle, {
      passive: !ns.includes("noPreventDefault"),
    });
  },
};

jQuery.event.special.wheel = {
  setup: function (_, ns, handle) {
    this.addEventListener("wheel", handle, { passive: true });
  },
};

jQuery.event.special.mousewheel = {
  setup: function (_, ns, handle) {
    this.addEventListener("mousewheel", handle, { passive: true });
  },
};

var lastScrollTop = 0;

$(document).ready(function () {
  $(window).on("scroll", function () {
    if ($(window).scrollTop() > 50) {
      $("header .header").addClass("dark");
    } else {
      $("header .header").removeClass("dark");
    }
  });

  $(window).scroll(function (event) {
    var st = $(this).scrollTop();
    if (st > lastScrollTop) {
      if ($(window).scrollTop() > 50) {
        $("header .header").addClass("dark");
        $("header .header").addClass("hide");
      }
    } else {
      if ($("header .header").hasClass("hide")) {
        $("header .header").removeClass("hide");
      }
      if ($(window).scrollTop() < 50) {
        $("header .header").removeClass("dark");
      }
    }
    lastScrollTop = st;
  });

  openMobileMenu();

  $("a").on("click", function (e) {
    scrollToTarget(this, e);

    if ($("header").hasClass("is-active")) {
      $("header").toggleClass("is-active");
    }
  });

  $(window).scroll(function () {
    if ($(window).width() >= 768) {
      if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
        $(".whatsapp-bubble").addClass("bottom");
      } else {
        $(".whatsapp-bubble").removeClass("bottom");
      }
    }
  });
});

function scrollToTarget(element, e) {
  var section = $(element).attr("href");

  if (Array.from(section)[0] === "#") {
    e.preventDefault();

    $("html,body").animate(
      {
        scrollTop: $(section).offset().top,
      },
      "slow"
    );
  }
}

function openMobileMenu() {
  $(".menu-button").on("click", function () {
    $("header").toggleClass("is-active");
  });
}
