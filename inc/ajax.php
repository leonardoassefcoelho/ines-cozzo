<?php
function ines_cuzzo_sendmail()
{
    if (
        !isset($_POST['contact-nonce'])
        || !wp_verify_nonce($_POST['contact-nonce'], 'contact_form')
    ) {
        wp_send_json_error(null, 400);
    } else if (isset($_POST['city']) && $_POST['city'] !== "") {
        wp_send_json_error(null, 400);
    } else {
        $to = get_field('contact-email', 'options');
        $email = $_POST['email'];
        $name = $_POST['name'];
        $phone = $_POST['phone'];
        $subject = $_POST['subject'];
        $message = $_POST['message'];

        if ($email && $name && $phone && $subject && $message) {
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: ' . $email . "\r\n" .
                'Reply-To: ' . $email . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            $content = sprintf("<p><strong>Nome:</strong> %s</p><p><strong>Telefone:</strong> %s</p><p><strong>E-Mail:</strong> %s</p><p><strong>Mensagem:</strong> %s</p>", $name, $phone, $email, $message);

            $email_sent = wp_mail($to, $name . " - " . $subject, $content, $headers);

            if ($email_sent) {
                wp_send_json_success(null, 200);
            } else {
                wp_send_json_error(null, 400);
            }
        } else {
            wp_send_json_error(null, 400);
        }
    }

    die();
}

add_action('wp_ajax_nopriv_sendmail', 'ines_cuzzo_sendmail');
add_action('wp_ajax_sendmail', 'ines_cuzzo_sendmail');
