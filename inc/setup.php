<?php

function ines_cozzo_setup()
{
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
}

add_action('after_setup_theme', 'ines_cozzo_setup');

function ines_cozzo_enqueue()
{
	$template_directory_uri = get_template_directory_uri();

	wp_enqueue_script('slick', $template_directory_uri . '/assets/js/common/slick.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script('common-js', $template_directory_uri . '/assets/js/common/common.js');

	// CSS enqueues
	wp_enqueue_style('home-css', $template_directory_uri . '/assets/css/home.css');

	//JS enqueues
	wp_enqueue_script('home-js', $template_directory_uri . '/assets/js/home/home.js');
}

add_action('wp_enqueue_scripts', 'ines_cozzo_enqueue');

function ines_cozzo_register_menus()
{
	register_nav_menus(array(
		'header-menu' => 'Header Menu',
	));
}

add_action('init', 'ines_cozzo_register_menus');

function ines_cozzo_set_post_views($post_id)
{
	$count_key = 'post_view_count';
	$count = get_post_meta($post_id, $count_key, true);

	if ($count == '') {
		add_post_meta($post_id, $count_key, '0');
	} else {
		$count = intval($count);
		$count++;
		update_post_meta($post_id, $count_key, $count);
	}
}

function ines_cozzo_track_post_views()
{
	if (is_single()) {
		$post_id = get_the_ID();
		ines_cozzo_set_post_views($post_id);
	}
}

add_action('wp_head', 'ines_cozzo_track_post_views');

function ines_cozzo_yoast_to_bottom()
{
	return 'low';
}

add_filter('wpseo_metabox_prio', 'ines_cozzo_yoast_to_bottom');
