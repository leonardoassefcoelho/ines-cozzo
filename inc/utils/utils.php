<?php
require_once 'functions/acf-options.php';
require_once 'functions/image-sanitizer.php';

//Adds support for safe SVG Files.
function ines_cozzo_add_svg_support($mimes)
{
    $mimes['svg'] = 'image/svg+xml';

    return $mimes;
}

add_filter('upload_mimes', 'ines_cozzo_add_svg_support');

//Removes WP Emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');

//REMOVER BARRA
add_filter('show_admin_bar', '__return_false');

add_filter('jetpack_sharing_counts', '__return_false', 99);
add_filter('jetpack_implode_frontend_css', '__return_false', 99);

add_filter( 'get_site_icon_url', '__return_false' );