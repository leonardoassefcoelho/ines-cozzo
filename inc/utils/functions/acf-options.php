<?php

function ines_cozzo_register_options_page() {
	if (function_exists('acf_add_options_page')) {
		acf_add_options_page(array(
			'page_title'    => 'Configurações do Site',
			'menu_title'    => 'Configurações do Site',
			'menu_slug'     => 'site-settings',
			'icon_url'      => 'dashicons-admin-settings'
		));
		
		acf_add_options_page(array(
			'page_title' 	=> 'Geral',
			'menu_title'	=> 'Geral',
			'menu_slug' 	=> 'geral-settings',
			'icon_url'		=> 'dashicons-text-page',
			'parent_slug'   => 'site-settings',
		));

		acf_add_options_page(array(
			'page_title' 	=> 'Redes Sociais',
			'menu_title'	=> 'Redes Sociais',
			'menu_slug' 	=> 'social-settings',
			'icon_url'		=> 'dashicons-text-page',
			'parent_slug'   => 'site-settings'
		));

		acf_add_options_page(array(
			'page_title'    => 'Google Tag Manager',
			'menu_title'    => 'GTM',
			'menu_slug'     => 'gtm-settings',
			'icon_url'      => 'dashicons-admin-settings',
			'parent_slug'   => 'site-settings',
		));
	}
}

add_action('after_setup_theme', 'ines_cozzo_register_options_page');
