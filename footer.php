        <?php wp_footer(); ?>
        <footer class="padding-container">
            <?php
            $header_logo = get_field("header-logo", "option");

            if ($header_logo) :
                echo file_get_contents($header_logo);
            endif;
            ?>
            <?php
            $template_directory_uri = get_template_directory_uri();
            $youtube_link = get_field('social-network-link-youtube', 'options');
            $linkedin_link = get_field('social-network-link-linkedin', 'options');
            $facebook_link = get_field('social-network-link-facebook', 'options');
            $instagram_link = get_field('social-network-link-instagram', 'options');
            $twitter_link = get_field('social-network-link-twitter', 'options');
            ?>
            <div class="social-wrapper">
                <?php if ($youtube_link && $youtube_link != "") : ?>
                    <a aria-label="Youtube" href="<?= $youtube_link ?>" target="_blank" rel="nofollow">
                        <?= file_get_contents($template_directory_uri . "/assets/icons/youtube.svg") ?>
                    </a>
                <?php endif; ?>
                <?php if ($linkedin_link && $linkedin_link != "") : ?>
                    <a aria-label="Linkedin" href="<?= $linkedin_link ?>" target="_blank" rel="nofollow">
                        <?= file_get_contents($template_directory_uri . "/assets/icons/linkedin.svg") ?>
                    </a>
                <?php endif; ?>
                <?php if ($facebook_link && $facebook_link != "") : ?>
                    <a aria-label="Facebook" href="<?= $facebook_link ?>" target="_blank" rel="nofollow">
                        <?= file_get_contents($template_directory_uri . "/assets/icons/facebook.svg") ?>
                    </a>
                <?php endif; ?>
                <?php if ($instagram_link && $instagram_link != "") : ?>
                    <a aria-label="Instagram" href="<?= $instagram_link ?>" target="_blank" rel="nofollow">
                        <?= file_get_contents($template_directory_uri . "/assets/icons/instagram.svg") ?>
                    </a>
                <?php endif; ?>
                <?php if ($twitter_link && $twitter_link != "") : ?>
                    <a aria-label="Twitter" href="<?= $twitter_link ?>" target="_blank" rel="nofollow">
                        <?= file_get_contents($template_directory_uri . "/assets/icons/twitter.svg") ?>
                    </a>
                <?php endif; ?>
            </div>
            <div class="footer-end-wrapper padding-container">
                <p>Ines Cozzo © <?= date('Y') ?></p>
                <p> Todos os Direitos Reservados.</p>
            </div>
        </footer>
        <div class="overlay" hidden></div>
        </body>

        </html>