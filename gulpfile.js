const gulp = require("gulp"),
  babel = require("gulp-babel"),
  { watch, series } = require("gulp");

var sass = require("gulp-sass")(require("sass"));

const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const sourcemaps = require("gulp-sourcemaps");
const log = require("fancy-log");

gulp.task("css", function () {
  return gulp.src("./src/*.css").pipe(postcss()).pipe(gulp.dest("./dest"));
});

gulp.task("javascript", () =>
  gulp
    .src("./assets/js-dev/**/*.js")
    .pipe(
      babel({
        presets: ["@babel/env"],
        compact: true,
        ignore: [
          "./assets/js-dev/single/clipboard.js",
          "./assets/js-dev/stockists/ol.js",
          "./assets/js-dev/common/slick.js",
        ],
      })
    )
    .pipe(gulp.dest("./assets/js/"))
);

gulp.task("scss", function (done) {
  var plugins = [autoprefixer(), cssnano()];

  gulp
    .src("./assets/scss/*.scss")
    .pipe(sourcemaps.init())
    .pipe(
      sass().on("error", function (err) {
        log.error(err.message);
        this.emit("end");
      })
    )
    .pipe(postcss(plugins))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./assets/css/"))
    .on("end", done);
});

exports.hardStart = series("javascript", "scss");

exports.default = function () {
  watch("./assets/js-dev/**/*.js", series("javascript"));
  watch("./assets/scss/**/*.scss", series("scss"));
};
